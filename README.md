# Smartlogic Code Test


## Getting Started

These instructions will get you a copy of the project running on your local machine.

### Prerequisites

Java 1.8 or higher

Please note that this program runs on MacOSX or linux. For windows the file paths in the code must be changed to match a windows file system.


### Running the Program from the command line

From the command line cd into the /src directory of this project

For example if you have unzipped the .zip file on your desktop:

```
$ cd ~/Desktop/SmartLogicCodeTest/src
```

Next run the command:

```
$ javac Demo.java
```
Then the command

```
$ java Demo
```

If the program ran correctly with no errors you should see:

```
Program Completed. Please Check output.txt
```

Check the contents of the generated output.txt file in the /src directory to verify results.



## Authors

* **Sean King**   6/6/2018



