/********************************
* Person.java
* 
* Representation of Person object
* 
* Created by Sean King  6/6/2018
*********************************/

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Person {
	
	private String firstName, lastName, middleInnitial, gender, favoriteColor;
	private GregorianCalendar dateOfBirth;
	
	Person(String lastName, String firstName, 
			String gender, GregorianCalendar dateOfBirth, String favoriteColor){
		
		this.lastName = lastName;
		this.firstName = firstName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.favoriteColor = favoriteColor;
	}
	
	public String getFirstName(){ return this.firstName; }
	public String getLastName(){ return this.lastName; }
	public String getGender(){ return this.gender;}
	public String getFavoriteColor(){ return this.favoriteColor; }
	public String getMiddleInitial(){ return this.middleInnitial; }
	public GregorianCalendar getDateOfBirth(){ return this.dateOfBirth; }
	
	public void setFirstName(String firstName){	this.firstName = firstName;	}
	public void setLastName(String lastName){ this.lastName = lastName; }
	public void setGender(String gender){ this.gender = gender; }
	public void setFavoriteColor(String favoriteColor){ this.favoriteColor = favoriteColor; }
	public void setMiddleInitial(String middleInnitial){ this.middleInnitial = middleInnitial; }
	public void setDateOfBirth(GregorianCalendar dateOfBirth){ this.dateOfBirth = dateOfBirth; }
	
	
	/**********************************************
	 * Fields are displayed in the following order:
	 * 1. last name
	 * 2. first name
	 * 3. gender
	 * 4. date of birth
	 * 5. favorite color
	 * 
	 * dates displayed in the format M/D/YYYY.
	 **********************************************/
	public String toString() {
	
		SimpleDateFormat fmt = new SimpleDateFormat("M/d/yyyy");
	    fmt.setCalendar(dateOfBirth);
	    String dateFormatted = fmt.format(dateOfBirth.getTime());
	   
		return  lastName 
				+ " " + firstName
				+ " " + gender 
				+ " " + dateFormatted 
				+ " " + favoriteColor;
	}
	
}
