/********************************************************************************
* ProcessOutput.java
* 
* Handles comparing Person objects for each of the three output orderings,
* and writing output to ouput.txt file 
* 
* Output 1 – sorted by gender (females before males) then by last name ascending
* Output 2 – sorted by birth date, ascending then by last name ascending
* Output 3 – sorted by last name, descending
* 
* Created by Sean King  6/6/2018
********************************************************************************/

import java.io.*;
import java.util.Comparator;
import java.util.List;

public class ProcessOutput {
	
	private Writer writer = null;
	private PrintWriter out = null;
	private List<Person> people;
	
	ProcessOutput(List<Person> people){
		this.people = people;
	}
	
	
	public void process() {
		
		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream("./output.txt"), "utf-8"));
		    
		    out = new PrintWriter(writer);
		    
		    //Output 1 – sorted by gender (females before males) then by last name ascending
		    out.println("Output 1: ");
		    people.stream()
	            .sorted(Comparator
	                    .comparing(Person::getGender)
	                    .thenComparing(Person::getLastName))
	            .forEach(out::println);

		    //Output 2 – sorted by birth date, ascending then by last name ascending
		    out.println("\nOutput 2: ");
		    people.stream()
	            .sorted(Comparator
	                    .comparing(Person::getDateOfBirth)
	                    .thenComparing(Person::getLastName))
	            .forEach(out::println);
		    
		    //Output 3 – sorted by last name, descending
		    out.println("\nOutput 3: ");
		    people.stream()
			    .sorted(Comparator
			    		.comparing(Person::getLastName)
			    		.reversed())
			    .forEach(out::println);
		    
		}catch (IOException e) {
			e.printStackTrace();		
			
		}finally {
		   
			try {
			   out.close();
			   writer.close();
			   
		   } catch (Exception e) {/*ignore*/}
		   
		}	
	}
}
