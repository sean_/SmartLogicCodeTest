/*****************************************
* Demo.java
* 
* Contains Main Method to run the program
* 
* Created by Sean King  6/6/2018
******************************************/

import java.util.ArrayList;
import java.util.List;

public class Demo {

	public static void main(String[] args) {
		
		List<Person> people = new ArrayList<Person>();
		
		ProcessInput space = new ProcessInput("./space.txt", " ", "-");
		ProcessInput comma = new ProcessInput("./comma.txt", ", ", "/");
		ProcessInput pipe = new ProcessInput("./pipe.txt", " \\| ", "-");
		
		people.addAll(space.getPersonList());
		people.addAll(comma.getPersonList());
		people.addAll(pipe.getPersonList());
		
		ProcessOutput processOutput = new ProcessOutput(people);
		processOutput.process();
		
		System.out.println("Program Completed. Please Check output.txt");
		
	}
}
