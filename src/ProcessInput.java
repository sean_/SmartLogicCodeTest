/*****************************************************************
* ProcessInput.java
* 
* Handles three types of input textfiles (space, comma, and pipe)
* Creates a list of Person objects from desired input files
* 
* Created by Sean King  6/6/2018
*****************************************************************/

import java.io.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

public class ProcessInput {
	
	private List<Person> people = new ArrayList<Person>();
	private Person person;
	private String firstName, lastName, gender, 
					favoriteColor, middleInnitial, 
					birthDate, field;
	private Scanner scanner, personScan;
	private GregorianCalendar dateOfBirth;
	private int birthMonth, birthDay, birthYear;
	
	/****************************************************************************************
	 * ProcessInput Constructor
	 *            - Creates a list of Person objects from input files according to parameters
	 * 
	 * @param filepath - file path to input file to process
	 * @param delimiter - delimiter used for type of input file 
	 * @param dateSeperator - dateSeperator(either "-" or "/") used for type of input file 
	 ****************************************************************************************/
	ProcessInput(String filepath, String delimiter, String dateSeperator){
		try {
			scanner = new Scanner(new File(filepath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		while(scanner.hasNext()) {
			
			field = scanner.nextLine();
			personScan = new Scanner(field);
			
			personScan.useDelimiter(delimiter);
			getAttributes(delimiter);
		
			dateOfBirth = stringToBirthday(dateSeperator);

			person = new Person(lastName, firstName, gender, dateOfBirth, favoriteColor);
			
			if(delimiter.equals(" ") || delimiter.equals(" \\| ")){
				person.setMiddleInitial(middleInnitial);
			}
			
			people.add(person);
		}
	}
	
	
	private void getAttributes(String delimiter) {
		
		if(delimiter.equals(" ")){
			getSpaceAttributes();	
			convertGender();
			
		}else if(delimiter.equals(" \\| ")){
			getPipeAttributes();
			convertGender();
			
		}else if(delimiter.equals(", ")) {
			getCommaAttributes();
		}
	}

	/*****************************************************************************
	 * The pipe-delimited file lists each record as follows:
	 * LastName | FirstName | MiddleInitial | Gender | FavoriteColor | DateOfBirth
	 *****************************************************************************/	
	private void getPipeAttributes() {
		while (personScan.hasNext()) {
			lastName = personScan.next();
			firstName = personScan.next();
			middleInnitial = personScan.next();
			gender = personScan.next();
			favoriteColor = personScan.next();
			birthDate = personScan.next();	
		}
	}

	/**********************************************************
	 * The comma-delimited file lists each record as follows:
	 * LastName, FirstName, Gender, FavoriteColor, DateOfBirth
	 **********************************************************/	
	private void getCommaAttributes() {
		while (personScan.hasNext()) {
			lastName = personScan.next();
			firstName = personScan.next();
			gender = personScan.next();
			favoriteColor = personScan.next();
			birthDate = personScan.next();	
		}
		
	}

	/**********************************************************
	 * The space-delimited lists each record as follows:
	 * LastName FirstName MiddleInitial Gender DateOfBirth FavoriteColor
	 **********************************************************/
	private void getSpaceAttributes() {
		while (personScan.hasNext()) {
			lastName = personScan.next();
			firstName = personScan.next();
			middleInnitial = personScan.next();
			gender = personScan.next();
			birthDate = personScan.next();
			favoriteColor = personScan.next();
		}
	}

	//Converts single letter representing gender to full word
	private void convertGender() {
		if(gender.equals("F")) {
			gender = "Female";
		}else if(gender.equals("M")){
			gender = "Male";
		}
	}
	
	//converts String birthdate depending on dateSeperator used to GregorianCalendar object
	private GregorianCalendar stringToBirthday(String dateSeperator) {
		String [] bdayArray = birthDate.split(dateSeperator);
		birthMonth = Integer.valueOf(bdayArray[0]) - 1; 
		birthDay = Integer.valueOf(bdayArray[1]); 
		birthYear = Integer.valueOf(bdayArray[2]); 
		
		return new GregorianCalendar(birthYear, birthMonth, birthDay);
	}
	
	//getter method to return list of Person objects
	public List<Person> getPersonList(){
		return people;
	}
}
